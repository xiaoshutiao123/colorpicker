package petrov.kristiyan.colorpicker_sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;
import petrov.kristiyan.colorpicker.ColorUtils;
import petrov.kristiyan.colorpicker.ResourceTable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {
//    @Test
//    public void testBundleName() {
//        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
//        assertEquals("petrov.kristiyan.colorpicker_sample", actualBundleName);
//    }

    @Test
    public void testIsWhiteText() {
        boolean isWhiteText = ColorUtils.isWhiteText(255);
        assertTrue(isWhiteText);
    }

    @Test
    public void testGetDimensionDp() {
        int isWhiteText = ColorUtils.getDimensionDp(
                ResourceTable.Float_action_button_min_width, MyApplication.getInstance());
        assertEquals(56, isWhiteText);
    }

    @Test
    public void testDip2px() {
        int isWhiteText = ColorUtils.dip2px(10, MyApplication.getInstance());
        assertEquals(30, isWhiteText);
    }

    @Test
    public void testRed() {
        int red = ColorUtils.red(255);
        assertEquals(0, red);
    }

    @Test
    public void testGreen() {
        int green = ColorUtils.green(255);
        assertEquals(0, green);
    }

    @Test
    public void testBlue() {
        int blue = ColorUtils.blue(255);
        assertEquals(255, blue);
    }
}