# colorpicker


#### 项目介绍
- 项目名称：colorpicker
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现设置颜色列表和选择颜色
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.1.10

#### 效果演示
<img src="img/colorpicker.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:colorpicker:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。

示例1：
```java
ColorPicker colorPicker = new ColorPicker(getAbility());
                    colorPicker.show();
                    colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                        @Override
                        public void onChooseColor(int position, int color) {
                            // put code
                        }

                        @Override
                        public void onCancel() {
                            // put code
                        }
                    });
```

示例2：
```java
final ColorPicker colorPicker = new ColorPicker(getAbility());
                    colorPicker.setOnFastChooseColorListener(new ColorPicker.OnFastChooseColorListener() {
                        @Override
                        public void setOnFastChooseColorListener(int position, int color) {
                            // put code
                        }

                        @Override
                        public void onCancel() {
                            // put code
                        }
                    })
                            .setDefaultColorButton(Color.getIntColor("#f84c44"))
                            .setColumns(5)
                            .show();
```

示例3：
```java
final ColorPicker colorPicker = new ColorPicker(getAbility());
                    colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                        @Override
                        public void onChooseColor(int position, int color) {
                            // put code
                        }

                        @Override
                        public void onCancel() {
                            // put code
                        }
                    })
                            .addListenerButton("newButton", new ColorPicker.OnButtonListener() {
                                @Override
                                public void onClick(Component v, int position, int color) {
                                    // put code
                                }
                            })
                            .disableDefaultButtons(true)
                            .setDefaultColorButton(Color.getIntColor("#f84c44"))
                            .setColumns(5)
                            .setDialogFullHeight()
                            .show();
```

选择自己的颜色
```java
setColors(int resId); //使用数组资源
setColors(ArrayList<String> colorsHexList); // 使用十六进制颜色列表
setColors(int... colorsList); // 使用颜色列表Color.RED，Color.Black等
setDefaultColorButton(int color); // 将colorButton设置为默认
```

定义侦听器
```java
setOnFastChooseColorListener(OnFastChooseColorListener listener);
setOnChooseColorListener(OnChooseColorListener listener);
```

添加自定义按钮
```java
addListenerButton(String text, Button button, OnButtonListener listener); // 自定义按钮
addListenerButton(String text, final OnButtonListener listener); // 它将生成具有默认样式的按钮
```

您可以使用的一般方法：
```java
setDialogFullHeight(); // 更大的高度
dismissDialog(); // 关闭对话框
setColumns(int c); // 设置列号
setTitle(String title); // 设置对话框的标题
setTitlePadding(int left, int top, int right, int bottom);
disableDefaultButtons(boolean disableDefaultButtons); // 如果要实现自己的按钮，请使用

getDialogBaseLayout(); // 返回用作对话框的基础的Layout 
getDialogViewLayout(); // 返回放大到对话框中的视图
getDialog(); // 返回对话框
getPositiveButton(); // 返回默认情况下定义的肯定按钮
getNegativeButton(); // 返回默认情况下定义的否定按钮
setDismissOnButtonListenerClick(boolean dismiss); 
```

您可以执行的ColorButtons更改：
```java
setColorButtonTickColor(int color); 
setColorButtonDrawable(int drawable);
setColorButtonSize(int width, int height);
setColorButtonMargin(int left, int top, int right, int bottom);
setRoundColorButton(boolean roundButton);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息

The MIT License

Copyright (c) 2016 Petrov Kristiyan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
