package petrov.kristiyan.colorpicker;

import ohos.aafwk.ability.Ability;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static petrov.kristiyan.colorpicker.ColorUtils.dip2px;
import static petrov.kristiyan.colorpicker.ColorUtils.getDimensionDp;

public class ColorPicker {
    private OnChooseColorListener onChooseColorListener;
    private OnFastChooseColorListener onFastChooseColorListener;

    public interface OnChooseColorListener {
        void onChooseColor(int position, int color);

        void onCancel();
    }

    public interface OnFastChooseColorListener {
        void setOnFastChooseColorListener(int position, int color);

        void onCancel();
    }

    public interface OnButtonListener {
        void onClick(Component v, int position, int color);
    }

    private ArrayList<ColorPal> colors;
    private ColorViewAdapter colorViewAdapter;
    private boolean fastChooser;
    private List<Integer> ta;
    private WeakReference<Ability> mContext;
    private int columns;
    private String title;
    private int marginLeft, marginRight, marginTop, marginBottom;
    private int tickColor;
    private int marginColorButtonLeft, marginColorButtonRight, marginColorButtonTop, marginColorButtonBottom;
    private int colorButtonWidth, colorButtonHeight;
    private int colorButtonDrawable;
    private String negativeText, positiveText;
    private boolean roundColorButton;
    private boolean dismiss;
    private boolean fullHeight;
    private WeakReference<CustomDialog> mDialog;
    private ListContainer listContainer;
    private DependentLayout colorpicker_base;
    private DirectionalLayout buttons_layout;
    private int default_color;
    private int paddingTitleLeft, paddingTitleRight, paddingTitleBottom, paddingTitleTop;
    private Component dialogViewLayout;
    private boolean disableDefaultButtons;
    private Button positiveButton, negativeButton;

    private List<Integer> getDefaultColorList() {
        List<Integer> colorList = new ArrayList<>();
        colorList.add(Color.getIntColor("#2062af"));
        colorList.add(Color.getIntColor("#58AEB7"));
        colorList.add(Color.getIntColor("#F4B528"));
        colorList.add(Color.getIntColor("#DD3E48"));
        colorList.add(Color.getIntColor("#BF89AE"));
        colorList.add(Color.getIntColor("#5C88BE"));
        colorList.add(Color.getIntColor("#59BC10"));
        colorList.add(Color.getIntColor("#E87034"));
        colorList.add(Color.getIntColor("#f84c44"));
        colorList.add(Color.getIntColor("#8c47fb"));
        colorList.add(Color.getIntColor("#51C1EE"));
        colorList.add(Color.getIntColor("#8cc453"));
        colorList.add(Color.getIntColor("#C2987D"));
        colorList.add(Color.getIntColor("#CE7777"));
        colorList.add(Color.getIntColor("#9086BA"));

        return colorList;
    }

    /**
     * Constructor
     *
     * @param context
     */
    public ColorPicker(Ability context) {
        dialogViewLayout = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_color_palette_layout, null, false);
        colorpicker_base = (DependentLayout) dialogViewLayout.findComponentById(ResourceTable.Id_colorpicker_base);
        listContainer = (ListContainer) dialogViewLayout.findComponentById(ResourceTable.Id_color_palette);
        buttons_layout = (DirectionalLayout) dialogViewLayout.findComponentById(ResourceTable.Id_buttons_layout);
        positiveButton = (Button) dialogViewLayout.findComponentById(ResourceTable.Id_positive);
        negativeButton = (Button) dialogViewLayout.findComponentById(ResourceTable.Id_negative);

        this.mContext = new WeakReference<>(context);
        this.dismiss = true;
        this.marginColorButtonLeft = 5;
        this.marginColorButtonTop = 5;
        this.marginColorButtonRight = 5;
        this.marginColorButtonBottom = 5;
        this.title = context.getString(ResourceTable.String_colorpicker_dialog_title);
        this.negativeText = context.getString(ResourceTable.String_colorpicker_dialog_cancel);
        this.positiveText = context.getString(ResourceTable.String_colorpicker_dialog_ok);
        this.default_color = 0;
        this.columns = 5;
    }

    /**
     * Set buttons color using a resource array of colors example : check in library  res/values/colorpicker-array.xml
     * 使用颜色资源数组设置按钮颜色示例：check in library res/values/colorpicker-array.xml
     *
     * @param resId Array resource
     * @return this
     */
    public ColorPicker setColors(int resId) {
        if (mContext == null) {
            return this;
        }

        Context context = mContext.get();
        if (context == null) {
            return this;
        }

        ta = getDefaultColorList();
        colors = new ArrayList<>();
        for (int i = 0; i < ta.size(); i++) {
            colors.add(new ColorPal(ta.get(i), false));
        }
        return this;
    }

    /**
     * Set buttons from an arraylist of Hex values
     *
     * @param colorsHexList List of hex values of the colors
     * @return this
     */
    public ColorPicker setColors(ArrayList<String> colorsHexList) {
        colors = new ArrayList<>();
        for (int i = 0; i < colorsHexList.size(); i++) {
            colors.add(new ColorPal(Color.getIntColor(colorsHexList.get(i)), false));
        }
        return this;
    }

    /**
     * Set buttons color  Example : Color.RED,Color.BLACK
     *
     * @param colorsList list of colors
     * @return this
     */
    public ColorPicker setColors(int... colorsList) {
        colors = new ArrayList<>();
        for (int aColorsList : colorsList) {
            colors.add(new ColorPal(aColorsList, false));
        }
        return this;
    }

    /**
     * Choose the color to be selected by default
     *
     * @param color int
     * @return this
     */
    public ColorPicker setDefaultColorButton(int color) {
        this.default_color = color;
        return this;
    }

    /**
     * Show the Material Dialog
     */
    public void show() {
        if (mContext == null) {
            return;
        }

        Ability context = mContext.get();
        if (context == null) {
            return;
        }

        if (colors == null || colors.isEmpty()) {
            setColors();
        }

        Text titleView = (Text) dialogViewLayout.findComponentById(ResourceTable.Id_title);
        if (title != null) {
            titleView.setText(title);
            titleView.setPadding(
                    dip2px(paddingTitleLeft, context), dip2px(paddingTitleTop, context),
                    dip2px(paddingTitleRight, context), dip2px(paddingTitleBottom, context));
        }
        mDialog = new WeakReference<>(new CustomDialog(context, dialogViewLayout));
        mDialog.get().setAutoClosable(true);

        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(columns);
        listContainer.setLayoutManager(tableLayoutManager);
        listContainer.setLongClickable(false);
        if (fastChooser) {
            colorViewAdapter = new ColorViewAdapter(mContext.get(), colors, onFastChooseColorListener, mDialog);
        } else {
            colorViewAdapter = new ColorViewAdapter(mContext.get(), colors);
        }

        if (fullHeight) {
            DependentLayout.LayoutConfig lp = new DependentLayout.LayoutConfig(DependentLayout.LayoutConfig.MATCH_CONTENT, DependentLayout.LayoutConfig.MATCH_PARENT);
            lp.addRule(DependentLayout.LayoutConfig.BELOW, titleView.getId());
            lp.addRule(DependentLayout.LayoutConfig.HORIZONTAL_CENTER, DependentLayout.LayoutConfig.TRUE);
            listContainer.setLayoutConfig(lp);
        }

        listContainer.setItemProvider(colorViewAdapter);

        if (marginBottom != 0 || marginLeft != 0 || marginRight != 0 || marginTop != 0) {
            colorViewAdapter.setMargin(marginLeft, marginTop, marginRight, marginBottom);
        }
        if (tickColor != 0) {
            colorViewAdapter.setTickColor(tickColor);
        }
        if (marginColorButtonBottom != 0 || marginColorButtonLeft != 0 || marginColorButtonRight != 0 || marginColorButtonTop != 0) {
            colorViewAdapter.setColorButtonMargin(
                    dip2px(marginColorButtonLeft, context), dip2px(marginColorButtonTop, context),
                    dip2px(marginColorButtonRight, context), dip2px(marginColorButtonBottom, context));
        }
        if (colorButtonHeight != 0 || colorButtonWidth != 0) {
            colorViewAdapter.setColorButtonSize(dip2px(colorButtonWidth, context), dip2px(colorButtonHeight, context));
        }
        if (roundColorButton) {
            setColorButtonDrawable(ResourceTable.Graphic_round_button);
        }
        if (colorButtonDrawable != 0) {
            colorViewAdapter.setColorButtonDrawable(colorButtonDrawable);
        }

        if (default_color != 0) {
            colorViewAdapter.setDefaultColor(default_color);
        }

        if (disableDefaultButtons) {
            positiveButton.setVisibility(Component.HIDE);
            negativeButton.setVisibility(Component.HIDE);
        }

        positiveButton.setText(positiveText);
        negativeButton.setText(negativeText);

        positiveButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (onChooseColorListener != null && !fastChooser) {
                    onChooseColorListener.onChooseColor(colorViewAdapter.getColorPosition(), colorViewAdapter.getColorSelected());
                }
                if (dismiss) {
                    dismissDialog();
                    if (onFastChooseColorListener != null) {
                        onFastChooseColorListener.onCancel();
                    }
                }
            }
        });

        negativeButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (dismiss) {
                    dismissDialog();
                }
                if (onChooseColorListener != null) {
                    onChooseColorListener.onCancel();
                }
            }
        });

        if (mDialog == null) {
            return;
        }

        CommonDialog dialog = mDialog.get();

        if (dialog != null && !context.isTerminating()) {
            dialog.show();
        }

    }

    /**
     * Define the number of columns by default value= 3
     *
     * @param c Columns number
     * @return this
     */
    public ColorPicker setColumns(int c) {
        columns = c;
        return this;
    }

    /**
     * Define the title of the Material Dialog
     *
     * @param title Title
     * @return this
     */
    public ColorPicker setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Set tick color
     *
     * @param color Color
     * @return this
     */
    public ColorPicker setColorButtonTickColor(int color) {
        this.tickColor = color;
        return this;
    }

    /**
     * Set a single drawable for all buttons example : you can define a different shape ( then round or square )
     * 为所有按钮设置一个可绘制的图形示例：可以定义不同的形状（然后是圆形或方形）
     *
     * @param drawable Resource
     * @return this
     */
    public ColorPicker setColorButtonDrawable(int drawable) {
        this.colorButtonDrawable = drawable;
        return this;
    }

    /**
     * Set the buttons size in DP
     *
     * @param width  width
     * @param height height
     * @return this
     */
    public ColorPicker setColorButtonSize(int width, int height) {
        this.colorButtonWidth = width;
        this.colorButtonHeight = height;
        return this;
    }

    /**
     * Set the Margin between the buttons in DP is 10
     *
     * @param left   left
     * @param top    top
     * @param right  right
     * @param bottom bottom
     * @return this
     */
    public ColorPicker setColorButtonMargin(int left, int top, int right, int bottom) {
        this.marginColorButtonLeft = left;
        this.marginColorButtonTop = top;
        this.marginColorButtonRight = right;
        this.marginColorButtonBottom = bottom;
        return this;
    }

    /**
     * Set round button
     *
     * @param roundButton true if you want a round button
     * @return this
     */
    public ColorPicker setRoundColorButton(boolean roundButton) {
        this.roundColorButton = roundButton;
        return this;
    }

    /**
     * set a fast listener ( it shows a mDialog without buttons and the event fires as soon you select a color )
     * 设置一个快速侦听器（它显示一个没有按钮的mDialog，并且当您选择一种颜色时事件就会触发）
     *
     * @param listener OnFastChooseColorListener
     * @return this
     */
    public ColorPicker setOnFastChooseColorListener(OnFastChooseColorListener listener) {
        this.fastChooser = true;
        buttons_layout.setVisibility(Component.HIDE);
        this.onFastChooseColorListener = listener;
        dismissDialog();
        return this;
    }

    /**
     * set a listener for the color picked
     *
     * @param listener OnChooseColorListener
     * @return ColorPicker
     */
    public ColorPicker setOnChooseColorListener(OnChooseColorListener listener) {
        onChooseColorListener = listener;
        return this;
    }

    /**
     * Add a  Button
     *
     * @param text     title of button
     * @param button   button to be added
     * @param listener listener
     * @return this
     */
    public ColorPicker addListenerButton(String text, Button button, final OnButtonListener listener) {
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                listener.onClick(v, colorViewAdapter.getColorPosition(), colorViewAdapter.getColorSelected());
            }
        });
        button.setText(text);
        if (button.getComponentParent() != null) {
            buttons_layout.removeComponent(button);
        }
        buttons_layout.addComponent(button);
        return this;
    }

    /**
     * add a new Button using default style
     *
     * @param text     title of button
     * @param listener OnButtonListener
     * @return this
     */
    public ColorPicker addListenerButton(String text, final OnButtonListener listener) {
        if (mContext == null) {
            return this;
        }

        Context context = mContext.get();
        if (context == null) {
            return this;
        }

        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                96
        );

        params.setMargins(dip2px(10, context), 0, 0, 0);
        Button button = new Button(context);
        button.setMinWidth(getDimensionDp(ResourceTable.Float_action_button_min_width, context));
        button.setPadding(
                getDimensionDp(ResourceTable.Float_action_button_padding_horizontal, context) + dip2px(5, context), 0,
                getDimensionDp(ResourceTable.Float_action_button_padding_horizontal, context) + dip2px(5, context), 0);
        button.setTextSize(getDimensionDp(ResourceTable.Float_action_button_text_size, context), Text.TextSizeType.FP);
        try {
            ShapeElement pressedElement = new ShapeElement();
            pressedElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#F0F0F0")));
            ShapeElement emptyElement = new ShapeElement();
            emptyElement.setRgbColor(RgbColor.fromArgbInt(context.getResourceManager().getElement(ResourceTable.Color_window_background).getColor()));

            StateElement stateElement = new StateElement();
            stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, pressedElement);
            stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
            button.setBackground(stateElement);
            button.setTextColor(new Color(context.getResourceManager().getElement(ResourceTable.Color_black_de).getColor()));
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.fillInStackTrace();
        }

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                listener.onClick(v, colorViewAdapter.getColorPosition(), colorViewAdapter.getColorSelected());
            }
        });
        button.setText(text.toUpperCase());
        if (button.getComponentParent() != null) {
            buttons_layout.removeComponent(button);
        }

        buttons_layout.addComponent(button);
        button.setLayoutConfig(params);
        return this;
    }

    /**
     * set if to dismiss the mDialog or not on button listener click, by default is set to true
     *
     * @param dismiss boolean
     * @return this
     */
    public ColorPicker setDismissOnButtonListenerClick(boolean dismiss) {
        this.dismiss = dismiss;
        return this;
    }

    /**
     * set Match_parent to RecyclerView
     *
     * @return this
     */
    public ColorPicker setDialogFullHeight() {
        this.fullHeight = true;
        return this;
    }

    /**
     * getmDialog if you need more options
     *
     * @return CustomDialog
     */
    public CustomDialog getmDialog() {
        if (mDialog == null) {
            return null;
        }
        return mDialog.get();
    }

    /**
     * getDialogViewLayout is the view inflated into the mDialog
     *
     * @return View
     */
    public Component getDialogViewLayout() {
        return dialogViewLayout;
    }

    /**
     * getDialogBaseLayout which is the RelativeLayout that contains the RecyclerView
     *
     * @return RelativeLayout
     */
    public DependentLayout getDialogBaseLayout() {
        return colorpicker_base;
    }

    /**
     * get the default PositiveButton
     *
     * @return Button
     */
    public Button getPositiveButton() {
        return positiveButton;
    }

    /**
     * get the default NegativeButton
     *
     * @return Button
     */
    public Button getNegativeButton() {
        return negativeButton;
    }

    /**
     * dismiss the mDialog
     */
    public void dismissDialog() {
        if (mDialog == null) {
            return;
        }

        BaseDialog dialog = mDialog.get();
        if (dialog != null && dialog.isShowing()) {
            dialog.destroy();
        }
    }

    /**
     * disables the postive and negative buttons
     *
     * @param disableDefaultButtons boolean
     * @return this
     */
    public ColorPicker disableDefaultButtons(boolean disableDefaultButtons) {
        this.disableDefaultButtons = disableDefaultButtons;
        return this;
    }

    /**
     * set padding to the title in DP
     *
     * @param left   dp
     * @param top    dp
     * @param right  dp
     * @param bottom dp
     * @return this
     */
    public ColorPicker setTitlePadding(int left, int top, int right, int bottom) {
        paddingTitleLeft = left;
        paddingTitleRight = right;
        paddingTitleTop = top;
        paddingTitleBottom = bottom;
        return this;
    }

    /**
     * Set default colors defined in colorpicker-array.xml of the library
     * 设置库的colorpicker-array.xml中定义的默认颜色
     *
     * @return this
     */
    private ColorPicker setColors() {
        if (mContext == null) {
            return this;
        }

        Context context = mContext.get();
        if (context == null) {
            return this;
        }

        ta = getDefaultColorList();
        colors = new ArrayList<>();
        for (int i = 0; i < ta.size(); i++) {
            colors.add(new ColorPal(ta.get(i), false));
        }
        return this;
    }

    private ColorPicker setMargin(int left, int top, int right, int bottom) {
        this.marginLeft = left;
        this.marginRight = right;
        this.marginTop = top;
        this.marginBottom = bottom;
        return this;
    }
}
