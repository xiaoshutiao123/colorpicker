package petrov.kristiyan.colorpicker;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

public class CustomDialog extends CommonDialog {
    private Component view;

    public CustomDialog(Context context, Component layout) {
        super(context);
        view = layout;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        setContentCustomComponent(view);
        setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
    }
}
