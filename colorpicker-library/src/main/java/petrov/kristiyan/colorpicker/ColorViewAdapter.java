package petrov.kristiyan.colorpicker;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ColorViewAdapter extends BaseItemProvider {
    private final Context context;
    private ColorPicker.OnFastChooseColorListener onFastChooseColorListener;
    private final ArrayList<ColorPal> mDataset;
    private int colorPosition = -1;
    private int colorSelected;
    private int marginLeft, marginRight, marginTop, marginBottom;
    private int tickColor = Color.WHITE.getValue();
    private int marginButtonLeft = 0, marginButtonRight = 0, marginButtonTop = 3, marginButtonBottom = 3;
    private int buttonWidth = -1, buttonHeight = -1;
    private WeakReference<CustomDialog> mDialog;

    private void dismissDialog() {
        if (mDialog == null) {
            return;
        }
        CommonDialog dialog = mDialog.get();
        if (dialog != null && dialog.isShowing()) {
            dialog.destroy();
        }
    }

    public int getColorSelected() {
        return colorSelected;
    }

    public int getColorPosition() {
        return colorPosition;
    }

    public ColorViewAdapter(Context context, ArrayList<ColorPal> myDataset, ColorPicker.OnFastChooseColorListener onFastChooseColorListener, WeakReference<CustomDialog> dialog) {
        this.context = context;
        mDataset = myDataset;
        mDialog = dialog;
        this.onFastChooseColorListener = onFastChooseColorListener;
    }

    public ColorViewAdapter(Context context, ArrayList<ColorPal> myDataset) {
        this.context = context;
        mDataset = myDataset;
    }

    @Override
    public int getCount() {
        return mDataset == null ? 0 : mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        if (mDataset != null && position >= 0 && position < mDataset.size()) {
            return mDataset.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder holder;
        if (component == null) {
            component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_palette_item, null, false);
            holder = new ViewHolder(component);
            // 将获取到的子组件信息绑定到列表项的实例中
            component.setTag(holder);
        } else {
            // 从缓存中获取到列表项实例后，直接使用绑定的子组件信息进行数据填充。
            holder = (ViewHolder) component.getTag();
        }

        int color = mDataset.get(position).getColor();

        int textColor = ColorUtils.isWhiteText(color) ? Color.WHITE.getValue() : Color.BLACK.getValue();

        if (mDataset.get(position).isCheck()) {
            holder.colorItem.setText("✔");
        } else {
            holder.colorItem.setText("");
        }

        holder.colorItem.setTextColor(tickColor == Color.WHITE.getValue() ? new Color(textColor) : new Color(tickColor));

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.OVAL);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        holder.colorItem.setBackground(shapeElement);
        holder.colorItem.setTag(color);

        holder.mark.setTag(position);

        return component;
    }

    // 用于保存列表项中的子组件信息
    public class ViewHolder implements Component.ClickedListener {
        public Button colorItem;
        public Component mark;

        public ViewHolder(Component v) {
            //buttons settings
            colorItem = (Button) v.findComponentById(ResourceTable.Id_color);
            mark = v.findComponentById(ResourceTable.Id_mark);
            colorItem.setClickedListener(this);
            DirectionalLayout.LayoutConfig layoutParams = (DirectionalLayout.LayoutConfig) colorItem.getLayoutConfig();
            layoutParams.setMargins(marginButtonLeft, marginButtonTop, marginButtonRight, marginButtonBottom);
            if (buttonWidth != -1) {
                layoutParams.width = buttonWidth;
            }
            if (buttonHeight != -1) {
                layoutParams.height = buttonHeight;
            }

            //relative layout settings
            DirectionalLayout directionalLayout = (DirectionalLayout) v.findComponentById(ResourceTable.Id_directionalLayout);
            ComponentContainer.LayoutConfig lp = directionalLayout.getLayoutConfig();
            lp.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        }

        @Override
        public void onClick(Component component) {
            if (colorPosition != -1 && colorPosition != (int) mark.getTag()) {
                mDataset.get(colorPosition).setCheck(false);
                notifyDataSetItemChanged(colorPosition);
            }
            colorPosition = (int) mark.getTag();
            colorSelected = (int) component.getTag();
            mDataset.get((int) mark.getTag()).setCheck(true);
            notifyDataSetItemChanged(colorPosition);

            if (onFastChooseColorListener != null && mDialog != null) {
                onFastChooseColorListener.setOnFastChooseColorListener(colorPosition, colorSelected);
                dismissDialog();
            }
        }
    }

    /**
     * 设置margin
     *
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public void setMargin(int left, int top, int right, int bottom) {
        this.marginBottom = bottom;
        this.marginLeft = left;
        this.marginRight = right;
        this.marginTop = top;
    }

    public void setDefaultColor(int color) {
        for (int i = 0; i < mDataset.size(); i++) {
            ColorPal colorPal = mDataset.get(i);
            if (colorPal.getColor() == color) {
                colorPal.setCheck(true);
                colorPosition = i;
                notifyDataSetItemChanged(i);
                colorSelected = color;
            }
        }
    }

    /**
     * 设置颜色
     *
     * @param color
     */
    public void setTickColor(int color) {
        this.tickColor = color;
    }

    /**
     * setColorButtonMargin
     *
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public void setColorButtonMargin(int left, int top, int right, int bottom) {
        this.marginButtonLeft = left;
        this.marginButtonRight = right;
        this.marginButtonTop = top;
        this.marginButtonBottom = bottom;
    }

    /**
     * setColorButtonSize
     *
     * @param width
     * @param height
     */
    public void setColorButtonSize(int width, int height) {
        this.buttonWidth = width;
        this.buttonHeight = height;
    }

    /**
     * setColorButtonDrawable
     *
     * @param drawable
     */
    public void setColorButtonDrawable(int drawable) {
    }
}